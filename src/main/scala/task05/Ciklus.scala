package task05

object Ciklus extends App {
  /**
   * Ha ki akarjuk írni a számokat from-tól to-ig (elejét is, végét is),
   * akkor megtehetjük pl. így rekurzióval:
   */
  def printLoop( from: Int, to: Int ): Unit = { //nincs meaningful return type
    if( from > to ) () //nem "lépünk be a ciklusba", a visszaadott érték ()
    else {
      println( from )
      printLoop( from+1, to )
    }
  }

  /**
   * Ha olyan függvényt szeretnénk írni, mely 1-től printeli a számokat
   * to-ig, akkor ezt pl. megoldhatjuk egy belső függvénnyel:
   */
  def printLoop( to: Int ): Unit = {
    def printLoopInner( from: Int, to: Int ): Unit = {
      if( from > to ) ()
      else {
        println( from )
        printLoopInner( from+1, to )
      }
    }
    printLoopInner(1, to)
  }

  /**
   * Írjunk olyan függvényt, mely visszaadja az input n > 1 egész szám
   * legkisebb, 1-nél nagyobb osztóját! (pl. 4-re 2-t adjon, 6-ra szintén
   * 2-t, 7-re pedig 7-et).
   * Ehhez érdemes lehet szintén belső függvényt deklarálni.
   * Ha n <= 1, adjon vissza 1-et.
   */
  def legkisebbOszto( n: Int ): Int = ???

  /**
   * Írjunk olyan függvényt, mely visszaadja az input n >= 0
   * számhoz azt a legnagyobb x egész számot, melyre x * (x+1) <= n.
   * (pl. 7-re 2-t kellene visszaadjon, mert 2 * 3 = 6 <= 7,
   * de 3 * 4 = 12 > 7. )
   */
  def nemNegyzetgyok( n : Int ): Int = ???

  /**
   * Itt egy példa az "adjuk össze a számokat 1-től n-ig" függvényre,
   * ciklussal és összegző értékkel: a belső függvényben a ciklusváltozó
   * és az eddigi összeg lesznek a paraméterek.
   * Mindkettőt úgy updateljük a rekurzív hívásnál, ahogy a ciklusban
   * változnának.
   */
  def sum( n: Int ): Int = {
    def loop( i: Int, sum: Int ): Int = { //i a ciklusváltozó, sum az összegző
      if ( i > n ) sum //túlmentünk, adjuk vissza az összeget
      else loop( i+1, sum+i ) //sum-ot növeljük i-vel, i-t pedig eggyel
    }
    loop( 1, 0 ) //inicializálás: i=1, sum=0
  }

  /**
   * Írjunk olyan függvényt, ami összeadja from-tól to-ig az egész
   * számokat! Pl. sum(3,6) = 3+4+5+6 = 18.
   */
  def sum( from: Int, to: Int ): Int = ???

  /**
   * Írjunk olyan függvényt, ami visszaadja, hogy az input
   * szám prímszám-e! (Ha n kisebb, mint 2, akkor false.)
   */
  def isPrime( n: Int ): Boolean = ???

  /**
   * Írjunk olyan függvényt, ami visszaadja, hogy az input szám
   * nála kisebb osztóinak mennyi az összege! (Pl. 6-ra 1+2+3 = 6,
   * 9-re 1+3=4, 1-re 0 kell legyen az eredmény.)
   * Felhehetjük, hogy az input pozitív egész.
   */
  def osztoOsszeg( n: Int ): Int = ???


  /**
   * Írjunk olyan függvényt, ami kap két Intet, 1 <= from <= to,
   * és kiírja konzolra az összes olyan "a b" párt, ahol
   * from <= a <= to, a osztóösszege b és b osztóösszege a!
   * (Ezeket hívják "barátságos" számoknak.
   * Minden pár közt legyen egy szóköz és utánuk egy újsor karakter.
   * Pl. 220 és 284 barátságos számok.
   */
  def printKoolajVezetek( from: Int, to: Int ): Unit = ???
}
