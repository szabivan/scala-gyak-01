package sol02

/** objektumnevet általában így kapitalizálunk:
 *  minden szó nagybetűvel kezdődik
 */
object KatonakVonatok extends App {

  /** használhatunk ékezetet és még nagyon sok mindent függvények vagy értékek nevében.
   *  ilyet nem fogunk többet csinálni, de lehet.
   *  
   *  Írjunk egy kétváltozós "bennfoglalás" nevű függvényt,
   *  mely megkapja első argumentumként a katonák számát (garantáltan nemnegatív egész),
   *  második argumentumként, hogy egy-egy vonaton hány katona fér el (garantáltan pozitív egész),
   *  és kiszámítja, hogy hány vonatot tudunk teljesen feltölteni katonákkal!
   */
  def bennfoglalás( katonak: Int, vonatok: Int ): Int = katonak / vonatok
  // itt is ugyanúgy mint C-ben, két Int közt a / egészosztás

  /** többszavas föggvénynevet általában így kapitalizálunk: az első betű kicsi,
   *  a többi szókezdő betű nagy.
   *  
   *  Írj egy kétváltozós "szerezdMegMindet" nevű függvényt,
   *  mely ugyanilyen sorrendben és feltételek mellett kapja meg a katonák és a vonatok számát,
   *  és kiszámítja, hogy hány vonat kell ahhoz, hogy minden katonát elszállítsunk!
   */
  def szerezdMegMindet( katonak: Int, vonatok: Int ): Int = 
    katonak / vonatok + (if( katonak % vonatok == 0 ) 0 else 1)
    // az if-else is kifejezés, használható úgy, mint a ?: C-ben
    // (katonak + vonatok - 1) / vonatok // ezzel is kijön a matek

}
