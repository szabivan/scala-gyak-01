package task03

object MinMax extends App {
  /**
   * ez a függvény visszaadja a bejövő két Int közül a kisebbet
   * (ha egyenlőek, akkor persze mindegy, melyiket)
   *
   * Scalában nem feltétlenül kell kiírjuk azokat a típusokat, amiket
   * a fordító ki tud következtetni.
   */
  def min( x: Int, y: Int ) = if ( x < y ) x else y

  // írjuk meg a kétváltozós maxot
  def max( x: Int, y: Int ): Int = ???

  //írjuk meg a háromváltozós mint
  def min( x: Int, y: Int, z: Int ): Int = ???

  //írjuk meg a háromváltozós midet
  def mid( x: Int, y: Int, z: Int ): Int = ???

  //írjuk meg a háromváltozós többségi függvényt
  def majority( x: Boolean, y: Boolean, z: Boolean ): Boolean = ???

}
