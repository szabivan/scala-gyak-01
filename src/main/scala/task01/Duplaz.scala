package task01

object Duplaz extends App {
  /**
   * Ha az "egyet hozzáadás" függvényt akarnánk implementálni,
   * akkor kb. így tennénk.
   */
  def pluszegy( n: Int ): Int = n + 1

  /**
   * Ezt a függvényt fejezzük be, hogy az input dupláját
   * számítsa ki!
   */
  def dup( n: Int ): Int = ???

  /**
   * Ezt a függvényt fejezzük be, hogy az input négyzetét
   * számítsa ki! Itt figyeljünk a túlcsordulásra!
   */
  def sqr( n: Int ): Long = ???
}
