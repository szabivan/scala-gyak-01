package sol04

object Szamlalas extends App {
  /**
   * A szokásos kötelező faktoriális példa.
   * Note: rekurzív függvénynek a típusát mindenképp ki kell írjuk,
   * mert a fordító nem tudja kikövetkeztetni.
   */
  def fakt( n: Int ): Long =
    if ( n == 0 ) 1
    else n * fakt( n - 1 )

  /**
   * Írjuk meg a másik kötelező rekurziós példát,
   * a Fibonacci számokkal:
   * - a 0. és az 1. Fibonacci szám mindkettő az 1,
   * - a továbbiak meg az előző kettő összege.
   * Nem baj, ha ez most nem lesz a leghatékonyabb megvalósítás.
   */
  def fib( n: Int ): Int = fib( 0, 1, n )
  // ezt gyakon végül is nem így csináltuk, de csökkenti a duplikációt

  /**
   * Írjuk meg az általános Fibonacci sorozatot:
   * - input a, b, n,
   * - a sorozat 0. eleme a, első eleme b,
   * - a továbbiak meg az előző kettő összege,
   * számítsuk ki az n. elemet!
   */
  def fib( a: Int, b: Int, n: Int ): Int = {
    // hogy ne adjuk oda a kövi szintnek mindig ugyanazt az a-t és b-t,
    // írunk egy belső függvényt, ez látni fogja az eredeti argumentumokat
    def fibHelper( m: Int ): Int = m match { // match: hasonló, mint a switch
      case 0 => a // nem kell break, ez az a lesz az érték, ha ez a case matchel
      case 1 => b
      case _ => fibHelper( m - 1 ) + fibHelper( m - 2 )
    }
    // megírtuk a belső függvényt, de meg is kell hívni, itt jó a negatív check
    // azért itt, hogy ne a helper ellenőrizze sokszor, itt elég egyszer
    if( n < 0 ) 42 else fibHelper( n ) // ha negatív az input, legyen mondjuk 42
  }

  /**
   * két nemnegatív egész, a és b, legnagyobb közös osztójára (gcd)
   * az euklideszi algoritmus:
   * - ha a kisebb szám 0, akkor a másik szám lesz a gcd;
   * - ha nem, akkor vegyük a nagyobb számnak a kisebbel való
   *   osztási maradékát, legyen ez c; az eredmény a kisebb szám
   *   és c legnagyobb közös osztója lesz.
   *
   * Írjuk meg ezt is! Feltételezhetjük, hogy az input helyes:
   * a is és b is nemnegatív egészek.
   */
  def gcd( a: Int, b: Int ): Int = ???

}
