package task04

object Szamlalas extends App {
  /**
   * A szokásos kötelező faktoriális példa.
   * Note: rekurzív függvénynek a típusát mindenképp ki kell írjuk,
   * mert a fordító nem tudja kikövetkeztetni.
   */
  def fakt( n: Int ): Long =
    if ( n == 0 ) 1
    else n * fakt( n - 1 )

  /**
   * Írjuk meg a másik kötelező rekurziós példát,
   * a Fibonacci számokkal:
   * - a 0. és az 1. Fibonacci szám mindkettő az 1,
   * - a továbbiak meg az előző kettő összege.
   * Nem baj, ha ez most nem lesz a leghatékonyabb megvalósítás.
   */
  def fib( n: Int ): Int = ???

  /**
   * Írjuk meg az általános Fibonacci sorozatot:
   * - input a, b, n,
   * - a sorozat 0. eleme a, első eleme b,
   * - a továbbiak meg az előző kettő összege,
   * számítsuk ki az n. elemet!
   */
  def fib( a: Int, b: Int, n: Int ): Int = ???

  /**
   * két nemnegatív egész, a és b, legnagyobb közös osztójára (gcd)
   * az euklideszi algoritmus:
   * - ha a kisebb szám 0, akkor a másik szám lesz a gcd;
   * - ha nem, akkor vegyük a nagyobb számnak a kisebbel való
   *   osztási maradékát, legyen ez c; az eredmény a kisebb szám
   *   és c legnagyobb közös osztója lesz.
   *
   * Írjuk meg ezt is! Feltételezhetjük, hogy az input helyes:
   * a is és b is nemnegatív egészek.
   */
  def gcd( a: Int, b: Int ): Int = ???

}
