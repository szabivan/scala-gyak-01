package sol00

/**
 * Egy tetszőlegesen elnevezett objektumnak, mely extendeli az App traitet,
 * a törzsébe beírt kifejezések sorban kiértékelődnek.
 *
 */
object Hello extends App {
   /**
    * Értéket a val kulcssszóval deklarálhatunk:
    * val <neve> : <típusa> = <értéke>
    * formában.
    * Itt pl. egy szoveg nevű értéket deklaráltunk,
    * String (szöveg, karakterlánc) típussal és Hello! értékkel.
    *
    * Stringeket Scalában többféleképp megadhatunk, az egyik a ""
    * idézőjelek közé zárás.
    */
   val szoveg: String = "Hello!"

   /**
    * A println függvény a paraméteréül kapott Stringet
    * kiírja a konzolra (és visszaadja a Unit típusú () értéket eredményül).
    * Üt egy újsort is.
    *
    * Írassuk ki, hogy Hello Scala!
    *
    */
   println( "Hello Scala!" )
}
