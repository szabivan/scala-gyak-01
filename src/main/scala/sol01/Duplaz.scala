package sol01

object Duplaz extends App {
  /**
   * Ha az "egyet hozzáadás" függvényt akarnánk implementálni,
   * akkor kb. így tennénk.
   */
  def pluszegy( n: Int ): Int = n + 1

  /**
   * Ezt a függvényt fejezzük be, hogy az input dupláját
   * számítsa ki!
   */
  def dup( n: Int ): Int = 2 * n

  /**
   * Ezt a függvényt fejezzük be, hogy az input négyzetét
   * számítsa ki! Itt figyeljünk a túlcsordulásra!
   */
  def sqr( n: Int ): Long = n.toLong * n
  // az Int osztálynak van sok hasonló metódusa, ezzel pl Longgá tudjuk castolni
  // és a Long meg az Int közti szorzás eredménye már Long lesz, nem vesztünk
  // el jegyeket, mint ha csak n * n -t írnánk (ami két Int közti szorzás,
  // Int eredménnyel)
}
