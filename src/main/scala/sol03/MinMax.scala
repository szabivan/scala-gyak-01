package sol03

object MinMax extends App {
  /**
   * ez a függvény visszaadja a bejövő két Int közül a kisebbet
   * (ha egyenlőek, akkor persze mindegy, melyiket)
   *
   * Scalában nem feltétlenül kell kiírjuk azokat a típusokat, amiket
   * a fordító ki tud következtetni.
   */
  def min( x: Int, y: Int ) = if ( x < y ) x else y

  // írjuk meg a kétváltozós maxot
  def max( x: Int, y: Int ): Int = if ( x > y ) y else x

  //írjuk meg a háromváltozós mint
  def min( x: Int, y: Int, z: Int ): Int = min( min( x, y ), z )
  // kommentek: ha egy kifejezést többször is kiértékelnénk, tegyük el valba
  // és ne implementáljunk újra meglévő funkcionalitást
  // pl: if( min(x,y) < z ) z else min(x,y) kétszer számolja ki min(x,y)-t
  // és ugyanaz, mint maga a minnek az implementációja

  //írjuk meg a háromváltozós midet
  def mid( x: Int, y: Int, z: Int ): Int =
    min( max(x,y), max(y,z), max(x,z) ) // ez is jó, sok if lesz belőle mondjuk
  // másik megoldás:
  def mid2( x: Int, y: Int, z: Int ): Int = {
    val sortedData = Vector( x, y, z ).sorted // ez létrehoz egy Int vectort ezzel a három elemmel, majd a rendezett változatát adja vissza
    sortedData( 1 ) // ez pedig visszaadja a középső (0-1-2) elemét a rendezettnek
    // ha egy objektumnak van "apply" nevű metódusa, azt függvényként is tudjuk hívni
    // de ki is írhatjuk: sortedData.apply( 1 ) ugyanaz
    // a fordító errort dob ha apply nélkül (Vector(x,y,z).sorted)(0), mert
    // úgy értelmezi, hogy a sorted függvénynek az argumentuma a (0)
  }

  //írjuk meg a háromváltozós többségi függvényt
  def majority( x: Boolean, y: Boolean, z: Boolean ): Boolean = 
    ( x && ( y || z ) ) || (y && z)
  // &&: logikai és, ||: logikai vagy

}
