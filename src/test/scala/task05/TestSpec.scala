package task05

import org.scalatest.flatspec.AnyFlatSpec

import java.io.ByteArrayOutputStream

class CiklusTest extends AnyFlatSpec {
  "Ciklus" should "Legkisebb osztó teszt" in {
    def checkNumber(n: Int, actual: Int): Unit = {
      val res = Ciklus.legkisebbOszto(n)
      assert(actual == res, s"$n legkisebb osztója $actual kéne legyen, lett: $res")
    }

    checkNumber(12, 2)
    checkNumber(1, 1)
    checkNumber(0, 1)
    checkNumber(-10, 1)
    checkNumber(44, 2)
    checkNumber(15, 3)
    checkNumber(7, 7)
    checkNumber(19, 19)
    checkNumber(25, 5)
  }
  it should "Nem négyzetgyök teszt" in {
    def checkNumber(n: Int, actual: Int): Unit = {
      val res = Ciklus.nemNegyzetgyok(n)
      assert(actual == res, s"nemNegyzetgyok($n) ==  $actual kéne legyen, lett: $res")
    }
    checkNumber( 6, 2 )
    checkNumber( 0, 0 )
    checkNumber( 10, 2 )
    checkNumber( 12, 3 )
    checkNumber( 100, 9 )
    checkNumber( 1, 0 )
    checkNumber( 60, 7 )
  }
  it should "sum teszt" in {
    for( to <- 1 to 100; from <- 1 to to ) {
      val res = Ciklus.sum(from,to)
      val actual = ((to*(to+1))-(from*(from-1))) / 2
      assert( res == actual,
        s"sum($from,$to) == $actual kéne legyen, lett $res")
    }
  }
  it should "prime teszt" in {
    for( n <- -2 to 100 ) {
      val res = Ciklus.isPrime(n)
      val actual = if ( n < 2 ) false else (2 to n-1).forall( n % _ != 0 )
      assert( res == actual, s"isPrime($n) == $actual kéne legyen, lett $res")
    }
  }
  it should "osztoOsszeg teszt" in {
    for( n <- 1 to 200 ) {
      val res = Ciklus.osztoOsszeg(n)
      val actual = ((1 until n) filter (n%_ == 0)).sum
      assert( res == actual, s"osztoOsszeg($n) == $actual kéne legyen, lett $res")
    }
  }
  it should "barátságos számok teszt " in {
    val stream = new ByteArrayOutputStream()
    Console.withOut(stream){
      Ciklus.printKoolajVezetek(15000,50000)
    }
    val sep = System.lineSeparator()
    val res = stream.toString
    val actual = s"17296 18416${sep}18416 17296${sep}"
    assert( res == actual, s"15.000 és 50.000 közt egy barátságos számpár van, az output pedig ${res}")
  }
}
