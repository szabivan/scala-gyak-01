package task04
import org.scalatest.flatspec.AnyFlatSpec

class SzamlalasTest extends AnyFlatSpec {

  "Szamlalas" should "Fibonacci teszt" in {
    def checkNumber(n: Int, actual: Long): Unit = {
      val res = Szamlalas.fib(n)
      assert( res == actual , s"fib($n) == $actual kéne legyen, lett: $res" )
    }
    Vector( (0,1),(1,1),(2,2),(3,3),(4,5),(5,8),(14,610)) foreach {
      t => checkNumber(t._1,t._2)
    }
  }
  
  it should "Általános fibonacci teszt" in {
    def checkNumber(tuple:(Int,Int,Int,Int)): Unit = {
      val (a,b,n,actual) = tuple
      val res = Szamlalas.fib(a,b,n)
      assert( res == actual , s"fib($n) == $actual kéne legyen, lett: $res" )
    }
    Vector(
      (1,1,0,1),
      (0,0,10,0),
      (1,2,5,13),
      (2,2,3,6),
      (3,-1,4,3)
    ) foreach checkNumber
  }

  it should "GCD teszt" in {
    def checkGcd( a: Int, b: Int, actual: Int ): Unit = {
      val res = Szamlalas.gcd(a,b)
      assert( res == actual, s"gcd($a,$b) should be $actual, was $res")
    }
    for( (a,b,res) <- Vector( (2,16,2), (3,6,3), (12,15,3), (20,15,5), (8,0,8), (0,0,0))) {
      checkGcd(a,b,res)
    }
  }
}
