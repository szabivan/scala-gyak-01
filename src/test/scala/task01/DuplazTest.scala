package task01

import org.scalatest.flatspec.AnyFlatSpec

import scala.util.Random

class DuplazTest extends AnyFlatSpec {
  "Duplázás" should "konstansokra" in {
    assert( Duplaz.dup(4) == 8, "dup(4) == 8 kellene legyen!")
    assert( Duplaz.dup(0) == 0, "dup(0) == 0 kellene legyen!")
    assert( Duplaz.dup(-3) == -6, "dup(-3) == -6 kellene legyen!")
  }
  it should "1-től 100-ig" in {
    for( n <- 1 to 100 ) {
      val inputLong: Long = Random.nextInt()
      val res = Duplaz.sqr( inputLong.toInt )
      val actual = inputLong * inputLong
      assert( res == actual, s"sqr(${inputLong}) == $actual kéne legyen, lett $res")
    }
  }
}
