package task03
import org.scalatest.flatspec.AnyFlatSpec
import scala.util.Random

class MinMaxTest extends AnyFlatSpec {
  "MinMaxTest" should "max/2" in {
    for( i <- 1 to 100 ) {
      val x = Random.nextInt(20)
      val y = Random.nextInt(20)
      val res = MinMax.max(x,y)
      assert(
        res >= x && res >= y && (res == x || res == y),
        "max of " + x + " and " + y
      )
    }
  }
  it should "min/3" in {
    for( i <- 1 to 100 ) {
      val x = Random.nextInt(20)
      val y = Random.nextInt(20)
      val z = Random.nextInt(20)
      val res = MinMax.min(x,y,z)
      assert(
        res <= x && res <= y && res <= z && (res == x || res == y || res == z ),
        "min of " + x + " and " + y + " and " + z
      )
    }
  }

  it should "mid/3" in {
    for( i <- 1 to 100 ) {
      val x = Random.nextInt(20)
      val y = Random.nextInt(20)
      val z = Random.nextInt(20)
      val res = MinMax.mid(x,y,z)
      val inputVector = Vector(x,y,z)
      assert(
        inputVector.count( _ <= res ) >= 2
        && inputVector.count( _ >= res ) >= 2,
        s"mid of $x and $y and $z"
      )
    }
  }

  it should "majority" in {
    for( i <- 1 to 100 ) {
      val x = Random.nextBoolean()
      val y = Random.nextBoolean()
      val z = Random.nextBoolean()
      val res = MinMax.majority(x,y,z)
      val inputVector = Vector(x,y,z)
      assert(
        inputVector.count( _ == res ) >= 2,
        s"majority of $x and $y and $z"
      )
    }
  }

}
