package task02

import org.scalatest.flatspec.AnyFlatSpec
import KatonakVonatok.{bennfoglalás, szerezdMegMindet}

class KatonakVonatokTest extends AnyFlatSpec {
  "Katonák, vonatok" should "Bennfoglalás teszt" in {
    assert( bennfoglalás(4,3) == 1, "4 katona 3 férőhelyes vonatból 1-et tud megtölteni!")
    assert( bennfoglalás(5,2) == 2, "5 katona 2 férőhelyes vonatból 2-őt tud megtölteni!")
    assert( bennfoglalás(9,3) == 3, "9 katona 3 férőhelyes vonatból 3-at tud megtölteni!")
    assert( bennfoglalás(0,2) == 0, "0 katona 2 férőhelyes vonatból 0-át tud megtölteni!")
  }

  it should "Szerezd meg mindet teszt" in {
    assert( szerezdMegMindet(6,3) == 2, "6 katona 3 férőhelyes vonatból 2-vel szállítható el!")
    assert( szerezdMegMindet(5,2) == 3, "5 katona 2 férőhelyes vonatból 3-mal szállítható el!")
    assert( szerezdMegMindet(7,3) == 3, "7 katona 3 férőhelyes vonatból 3-mal szállítható el!")
    assert( szerezdMegMindet(0,1) == 0, "0 katona 1 férőhelyes vonatból 0-val szállítható el!")
  }

}
